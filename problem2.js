const fs = require('fs');

function fsCallbackProblem2(folderPath){
    const promise = readTxtfile("lipsum.txt");
    promise.then((data)=>{
        console.log("File is read");
        return createAndwriteFile(data.toUpperCase(),"upperCase.txt");
    }).then((result)=>{
        console.log(result);
        return updateFileNames("upperCase.txt");
    }).then((result)=>{
        console.log(result);
        return readTxtfile("upperCase.txt");
    }).then((data)=>{
        console.log("UpperCase.txt is read");
        return createAndwriteFile(data.toLowerCase().replaceAll(".",".\n"),"splitLowerCaseSentences.txt");
    }).then((result)=>{
        console.log(result);
        return updateFileNames("\nsplitLowerCaseSentences.txt");
    }).then((result)=>{
        console.log(result);
        return readTxtfile("splitLowerCaseSentences.txt");
    }).then((data)=>{
        console.log("splitLowerCaseSentences.txt is read");
        return createAndwriteFile(data.split("\n").sort().join("\n"),"sorted.txt");
    }).then((result)=>{
        console.log(result);
        return updateFileNames("\nsorted.txt");
    }).then((result)=>{
        console.log(result);
        return readTxtfile("fileNames.txt");
    }).then((data)=>{
        console.log("fileNames.txt is read");
        return deleteAll(data);
    }).then((result)=>{
        console.log(result);
    }).catch((err)=>{
        console.error(err);
    })

    function readTxtfile(file){
        return new Promise((resolve,reject)=>{
            fs.readFile(folderPath+file, 'utf-8',(err,data) =>{
                if(err) reject(err);
                else resolve(data);
            })
        })
    }

    function updateFileNames(fileName){
        return new Promise((resolve,reject)=>{
            fs.appendFile(folderPath+"fileNames.txt", fileName, err=>{
                if(err) reject(err);
                else resolve("fileNames.txt updated.");
            })
        })
    }

    function createAndwriteFile(data, fileName){
        return new Promise((resolve,reject)=>{
            fs.writeFile(folderPath+fileName, data, err =>{
                if(err) reject(err);
                else resolve(fileName+" is created and written");
            })
        })
    }

    function deleteAll(data){
        return new Promise((resolve,reject)=>{
            let deleteList = data.split("\n");
            for(let i in deleteList){
                fs.unlink(folderPath+deleteList[i],err=>{
                    if(err) reject(err);
                    else resolve(deleteList+" are deleted");
                })
            }
        })
    }
}

module.exports = fsCallbackProblem2;